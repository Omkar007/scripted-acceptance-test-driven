#!/usr/bin/python
from Employee import Employee

class Staff:

    def __init__(self):
        self.EmployeeList = []

    def addEmployee(self, EmployeeName, EmployeeID, ServiceYears, Department, Designation, Salary):
        employee_obj = Employee(EmployeeName, EmployeeID, ServiceYears,Department, Designation, Salary)
        #employee_obj.print_employee_details()
        self.EmployeeList.append(employee_obj)
        
    def updateSalary(self,employeeName,salary):
        if(int(salary) < 0):
            print('Salary to be updated cannot be negative. Kindly enter a valid Salary')    
            
        for emp in self.EmployeeList: 
            if(emp._EmployeeName is employeeName):
                emp._Salary = salary
                break                 
            else:
                print(employeeName + ' does not exist in the Employee List. Please Enter a existing Employee')
                break
    
    def printAllEmployees(self): 
        for emp in self.EmployeeList:
            print('Employee Name: ',emp._EmployeeName + ' EmployeeID: ',emp._EmployeeID+ ' Years Worked: ',
                  emp._ServiceYears + ' Department: ',emp._Department+ ' Designation: ',emp._Designation+ ' Salary: ',emp._Salary)


#Main function --

staff_obj = Staff()
print('Employees present in the list: ')
print()
staff_obj.addEmployee('John Doe','I-201','16','Accounts','Account Manager','95000')
staff_obj.addEmployee('Neil Jue','M-201','16','Marketing','Senior Manager','105000')
staff_obj.addEmployee('Anne Cox','S-201','16','Technology','Solution Architect','130000')
staff_obj.printAllEmployees()
print()
print('Trying to update salary of Employee which does not exist ')
staff_obj.updateSalary('Jonathan', '150000');
print()
print('Trying to update salary of Employee which is negative or not valid ')
staff_obj.updateSalary('John Doe', '-90000');
print()
print('Employee list after updating Employee John salary')
print()
staff_obj.updateSalary('John Doe', '98000');
staff_obj.printAllEmployees()