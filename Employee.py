class Employee:

    def __init__(self, EmployeeName , EmployeeID , ServiceYears,Department, Designation, Salary):
        self._EmployeeName = EmployeeName
        self._EmployeeID = EmployeeID
        self._ServiceYears = ServiceYears
        self._Department = Department
        self._Designation = Designation
        self._Salary = Salary

    def print_employee_details(self):
        print('Employee Name: ',self._EmployeeName + ' EmployeeID: ',self._EmployeeID+ 
              ' Years Worked: ',self._ServiceYears+ ' Salary: ',self._Salary)