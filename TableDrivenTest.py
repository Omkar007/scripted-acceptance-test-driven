#!/usr/bin/python

# base salary + 1 % sells - social security
def EmployeeWage(L):
    if not L:
        return None
    
    if(len(L) != 3) :
        return 'Please provide Employee Base Salary, His sales and social security'
    else:
        result = (L[0] + (L[1] / 100)) - L[2]
        return result

def test_wage():
    test_cases = [
        {
            "name": "Employee 1",
            "input": [1500,50000,20],
            "expected": 6.0
        },
        {
            "name": "Employee 2",
            "input": [1000, 20000,20],
            "expected": 10
        },
        {
            "name": "list with one item",
            "input": [100],
            "expected": 100.0
        },
        {
            "name": "empty list",
            "input": [],
            "expected": None
        }
    ]
  
    for test_case in test_cases:
        assert test_case["expected"] == EmployeeWage(test_case["input"]), test_case["name"]       